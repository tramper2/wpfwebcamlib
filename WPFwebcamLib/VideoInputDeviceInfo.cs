﻿using System;
using System.Runtime.InteropServices;

namespace WPFwebcamLib
{
    /// <summary>
    /// 비디오 입력 장치 정보
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct VideoInputDeviceInfo
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Field
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region Field

        /// <summary>
        /// 명칭
        /// </summary>
        [MarshalAs(UnmanagedType.BStr)]
        public String Name;

        /// <summary>
        /// 장치 경로
        /// </summary>
        [MarshalAs(UnmanagedType.BStr)]
        public String DevicePath;

        #endregion
    }
}