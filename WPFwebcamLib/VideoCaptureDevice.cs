﻿using System;

namespace WPFwebcamLib
{
    /// <summary>
    /// 비디오 캡처 디바이스
    /// </summary>
    public sealed class VideoCaptureDevice : IEquatable<VideoCaptureDevice>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Property
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 명칭 - Name

        /// <summary>
        /// 명칭
        /// </summary>
        public string Name { get; private set; }

        #endregion
        #region 장치 경로 - DevicePath

        /// <summary>
        /// 장치 경로
        /// </summary>
        public string DevicePath { get; private set; }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Constructor
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 생성자 - VideoCaptureDevice(info)

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="info">비디오 입력 장치 정보</param>
        public VideoCaptureDevice(VideoInputDeviceInfo info)
        {
            Name       = info.Name;
            DevicePath = info.DevicePath;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Method
        ////////////////////////////////////////////////////////////////////////////////////////// Static
        //////////////////////////////////////////////////////////////////////////////// Public

        #region == 연산자 재정의하기 - ==(left, right)

        /// <summary>
        /// == 연산자 재정의하기
        /// </summary>
        /// <param name="left">왼쪽 객체</param>
        /// <param name="right">오른쪽 객체</param>
        /// <returns>연산 결과</returns>
        public static bool operator ==(VideoCaptureDevice left, VideoCaptureDevice right)
        {
            return Equals(left, right);
        }

        #endregion
        #region != 연산자 재정의하기 - !=(left, right)

        /// <summary>
        /// != 연산자 재정의하기
        /// </summary>
        /// <param name="left">왼쪽 객체</param>
        /// <param name="right">오른쪽 객체</param>
        /// <returns>연산 결과</returns>
        public static bool operator !=(VideoCaptureDevice left, VideoCaptureDevice right)
        {
            return !Equals(left, right);
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////// Instance
        //////////////////////////////////////////////////////////////////////////////// Public

        #region 동일 여부 구하기 - Equals(sourceObject)

        /// <summary>
        /// 동일 여부 구하기
        /// </summary>
        /// <param name="sourceObject">소스 객체</param>
        /// <returns>동일 여부</returns>
        public override bool Equals(object sourceObject)
        {
            if(ReferenceEquals(null, sourceObject))
            {
                return false;
            }

            if(ReferenceEquals(this, sourceObject))
            {
                return true;
            }

            if(sourceObject.GetType() != typeof(VideoCaptureDevice))
            {
                return false;
            }

            return Equals((VideoCaptureDevice)sourceObject);
        }

        #endregion
        #region 동일 여부 구하기 - Equals(target)

        /// <summary>
        /// 동일 여부 구하기
        /// </summary>
        /// <param name="target">타겟 비디오 캡처 장치</param>
        /// <returns>동일 여부</returns>
        public bool Equals(VideoCaptureDevice target)
        {
            if(ReferenceEquals(null, target))
            {
                return false;
            }

            if(ReferenceEquals(this, target))
            {
                return true;
            }

            return Equals(target.Name, Name) && Equals(target.DevicePath, DevicePath);
        }

        #endregion

        #region 해시 코드 구하기 - GetHashCode()

        /// <summary>
        /// 해시 코드 구하기
        /// </summary>
        /// <returns>해시 코드</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Name.GetHashCode() * 397) ^ DevicePath.GetHashCode();
            }
        }

        #endregion
    }
}