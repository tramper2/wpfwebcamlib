﻿using System;

namespace WPFwebcamLib
{
    /// <summary>
    /// 윈도우 스타일
    /// </summary>
    [Flags]
    public enum WindowStyles : uint
    {
        /// <summary>
        /// WS_CHILD
        /// </summary>
        WS_CHILD = 0x40000000,

        /// <summary>
        /// WS_VISIBLE
        /// </summary>
        WS_VISIBLE = 0x10000000
    }
}