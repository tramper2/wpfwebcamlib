﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

using WPFwebcamLib.Properties;

namespace WPFwebcamLib
{
    /// <summary>
    /// DirectShow 헬퍼
    /// </summary>
    public sealed class DirectShowHelper : IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Import
        ////////////////////////////////////////////////////////////////////////////////////////// Static
        //////////////////////////////////////////////////////////////////////////////// Private

        #region 라이브러리 로드하기 - LoadLibrary(filePath)

        /// <summary>
        /// 라이브러리 로드하기
        /// </summary>
        /// <param name="filePath">파일 경로</param>
        /// <returns>처리 결과</returns>
        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr LoadLibrary(string filePath);

        #endregion
        #region 프로시저 주소 구하기 - GetProcAddress(moduleHandle, processName)

        /// <summary>
        /// 프로시저 주소 구하기
        /// </summary>
        /// <param name="moduleHandle">모듈 핸들</param>
        /// <param name="procedureName">프로시저명</param>
        /// <returns>프로세스 핸들</returns>
        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr moduleHandle, string procedureName);

        #endregion
        #region 라이브러리 해제하기 - FreeLibrary(moduleHandle)

        /// <summary>
        /// 라이브러리 해제하기
        /// </summary>
        /// <param name="moduleHandle">모듈 핸들</param>
        /// <returns>처리 결과</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeLibrary(IntPtr moduleHandle);

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Delegate
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 비디오 입력 장치 열거 콜백 처리하기 대리자 - EnumerateVideoInputDeviceCallbackDelegate(info)

        /// <summary>
        /// 비디오 입력 장치 열거 콜백 처리하기 대리자
        /// </summary>
        /// <param name="info">비디오 입력 장치 정보</param>
        public delegate void EnumerateVideoInputDeviceCallbackDelegate(ref VideoInputDeviceInfo info);

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////// Private

        #region 비디오 입력 장치 리스트 열거하기 대리자 - EnumerateVideoInputDeviceListDelegate(callback)

        /// <summary>
        /// 비디오 입력 장치 리스트 열거하기 대리자
        /// </summary>
        /// <param name="callback">비디오 입력 장치 열거 콜백 대리자</param>
        private delegate void EnumerateVideoInputDeviceListDelegate(EnumerateVideoInputDeviceCallbackDelegate callback);

        #endregion
        #region 캡처 그래프 구축하기 대리자 - BuildCaptureGraphDelegate()

        /// <summary>
        /// 캡처 그래프 구축하기 대리자
        /// </summary>
        /// <returns>처리 결과</returns>
        private delegate int BuildCaptureGraphDelegate();

        #endregion
        #region 렌더 필터 추가하기 대리자 - AddRenderFilterDelegate(windowHandle)

        /// <summary>
        /// 렌더 필터 추가하기 대리자
        /// </summary>
        /// <param name="windowHandle">윈도우 핸들</param>
        /// <returns>처리 결과</returns>
        private delegate int AddRenderFilterDelegate(IntPtr windowHandle);

        #endregion
        #region 캡처 필터 추가하기 대리자 - AddCaptureFilterDelegate(devicePath)

        /// <summary>
        /// 캡처 필터 추가하기 대리자
        /// </summary>
        /// <param name="devicePath">장치 경로</param>
        /// <returns>처리 결과</returns>
        private delegate int AddCaptureFilterDelegate([MarshalAs(UnmanagedType.BStr)]string devicePath);

        #endregion
        #region 캡처 그래프 리셋하기 대리자 - ResetCaptureGraphDelegate()

        /// <summary>
        /// 캡처 그래프 리셋하기 대리자
        /// </summary>
        /// <returns>처리 결과</returns>
        private delegate int ResetCaptureGraphDelegate();

        #endregion
        #region 시작하기 대리자 - StartDelegate()

        /// <summary>
        /// 시작하기 대리자
        /// </summary>
        /// <returns>처리 결과</returns>
        private delegate int StartDelegate();

        #endregion
        #region 현재 이미지 구하기 대리자 - GetCurrentImageDelegate(dibHandle)

        /// <summary>
        /// 현재 이미지 구하기 대리자
        /// </summary>
        /// <param name="dibHandle">DIB 핸들</param>
        /// <returns>처리 결과</returns>
        private delegate int GetCurrentImageDelegate([Out] out IntPtr dibHandle);

        #endregion
        #region 비디오 크기 구하기 대리자 - GetVideoSizeDelegate(width, height)

        /// <summary>
        /// 비디오 크기 구하기 대리자
        /// </summary>
        /// <param name="width">너비</param>
        /// <param name="height">높이</param>
        /// <returns>처리 결과</returns>
        private delegate int GetVideoSizeDelegate(out int width, out int height);

        #endregion
        #region 중단하기 대리자 - StopDelegate()

        /// <summary>
        /// 중단하기 대리자
        /// </summary>
        /// <returns>처리 결과</returns>
        private delegate int StopDelegate();

        #endregion
        #region 캡처 그래프 제거하기 대리자 - DestroyCaptureGraphDelegate()

        /// <summary>
        /// 캡처 그래프 제거하기 대리자
        /// </summary>
        private delegate void DestroyCaptureGraphDelegate();

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Field
        ////////////////////////////////////////////////////////////////////////////////////////// Private

        #region Field

        /// <summary>
        /// 비디오 입력 장치 리스트 열거하기 대리자
        /// </summary>
        private EnumerateVideoInputDeviceListDelegate enumerateVideoInputDeviceListDelegate;

        /// <summary>
        /// 캡처 그래프 구축하기 대리자
        /// </summary>
        private BuildCaptureGraphDelegate buildCaptureGraphDelegate;

        /// <summary>
        /// 렌더 필터 추가하기 대리자
        /// </summary>
        private AddRenderFilterDelegate addRenderFilterDelegate;

        /// <summary>
        /// 캡처 필터 추가하기 대리자
        /// </summary>
        private AddCaptureFilterDelegate addCaptureFilterDelegate;

        /// <summary>
        /// 캡처 그래프 리셋하기 대리자
        /// </summary>
        private ResetCaptureGraphDelegate resetCaptureGraphDelegate;

        /// <summary>
        /// 시작하기 대리자
        /// </summary>
        private StartDelegate startDelegate;

        /// <summary>
        /// 현재 이미지 구하기 대리자
        /// </summary>
        private GetCurrentImageDelegate getCurrentImageDelegate;

        /// <summary>
        /// 비디오 크기 구하기 대리자
        /// </summary>
        private GetVideoSizeDelegate getVideoSizeDelegate;

        /// <summary>
        /// 중단하기 대리자
        /// </summary>
        private StopDelegate stopDelegate;

        /// <summary>
        /// 캡처 그래프 제거하기 대리자
        /// </summary>
        private DestroyCaptureGraphDelegate destroyCaptureGraphDelegate;

        /// <summary>
        /// 모듈 파일 경로
        /// </summary>
        private string moduleFilePath = string.Empty;

        /// <summary>
        /// 모듈 핸들
        /// </summary>
        private IntPtr moduleHandle = IntPtr.Zero;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Property
        ////////////////////////////////////////////////////////////////////////////////////////// Private

        #region 32비트 플랫폼 여부 - IsX86Platform

        /// <summary>
        /// 32비트 플랫폼 여부
        /// </summary>
        private bool IsX86Platform
        {
            get
            {
                return IntPtr.Size == 4;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Constructor
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 생성자 - DirectShowHelper()

        /// <summary>
        /// 생성자
        /// </summary>
        public DirectShowHelper()
        {
            LoadModule();

            SetDelegate(this.moduleHandle);
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Method
        ////////////////////////////////////////////////////////////////////////////////////////// Public
        
        #region 비디오 입력 장치 리스트 열거하기 - EnumerateVideoInputDeviceList(enumerateVideoInputDeviceCallbackDelegate)

        /// <summary>
        /// 비디오 입력 장치 리스트 열거하기
        /// </summary>
        /// <param name="enumerateVideoInputDeviceCallbackDelegate">비디오 입력 장치 열거 콜백 처리하기 대리자</param>
        public void EnumerateVideoInputDeviceList(EnumerateVideoInputDeviceCallbackDelegate enumerateVideoInputDeviceCallbackDelegate)
        {
            this.enumerateVideoInputDeviceListDelegate(enumerateVideoInputDeviceCallbackDelegate);
        }

        #endregion

        #region 캡처 그래프 구축하기 - BuildCaptureGraph()

        /// <summary>
        /// 캡처 그래프 구축하기
        /// </summary>
        public void BuildCaptureGraph()
        {
            ThrowExceptionForResult(this.buildCaptureGraphDelegate(), "Failed to build a video capture graph.");
        }

        #endregion
        #region 렌더 필터 추가하기 - AddRenderFilter(windowHandle)

        /// <summary>
        /// 렌더 필터 추가하기
        /// </summary>
        /// <param name="windowHandle"></param>
        public void AddRenderFilter(IntPtr windowHandle)
        {
            ThrowExceptionForResult(this.addRenderFilterDelegate(windowHandle), "Failed to setup a render filter.");
        }

        #endregion
        #region 캡처 필터 추가하기 - AddCaptureFilter(devicePath)

        /// <summary>
        /// 캡처 필터 추가하기
        /// </summary>
        /// <param name="devicePath">장치 경로</param>
        public void AddCaptureFilter(string devicePath)
        {
            ThrowExceptionForResult(this.addCaptureFilterDelegate(devicePath), "Failed to add a video capture filter.");
        }

        #endregion
        #region 캡처 그래프 리셋하기 - ResetCaptureGraph()

        /// <summary>
        /// 캡처 그래프 리셋하기
        /// </summary>
        public void ResetCaptureGraph()
        {
            ThrowExceptionForResult(this.resetCaptureGraphDelegate(), "Failed to reset a video capture graph.");
        }

        #endregion
        #region 시작하기 - Start()

        /// <summary>
        /// 시작하기
        /// </summary>
        public void Start()
        {
            ThrowExceptionForResult(this.startDelegate(), "Failed to run a capture graph.");
        }

        #endregion
        #region 현재 이미지 구하기 - GetCurrentImage()

        /// <summary>
        /// 현재 이미지 구하기
        /// </summary>
        /// <returns>현재 이미지</returns>
        public Bitmap GetCurrentImage()
        {
            IntPtr dibHandle;

            ThrowExceptionForResult(this.getCurrentImageDelegate(out dibHandle), "Failed to get the current image.");

            try
            {
                BITMAPINFOHEADER bitmapInfoHeader = (BITMAPINFOHEADER)Marshal.PtrToStructure(dibHandle, typeof(BITMAPINFOHEADER));

                int stride = bitmapInfoHeader.Width * (bitmapInfoHeader.BitCount / 8);

                int padding = stride % 4 > 0 ? 4 - stride % 4 : 0;

                stride += padding;

                PixelFormat pixelFormat = PixelFormat.Undefined;

                switch(bitmapInfoHeader.BitCount)
                {
                    case 1  : pixelFormat = PixelFormat.Format1bppIndexed; break;
                    case 4  : pixelFormat = PixelFormat.Format4bppIndexed; break;
                    case 8  : pixelFormat = PixelFormat.Format8bppIndexed; break;
                    case 16 : pixelFormat = PixelFormat.Format16bppRgb555; break;
                    case 24 : pixelFormat = PixelFormat.Format24bppRgb;    break;
                    case 32 : pixelFormat = PixelFormat.Format32bppRgb;    break;
                }

                Bitmap bitmap = new Bitmap
                (
                    bitmapInfoHeader.Width,
                    bitmapInfoHeader.Height,
                    stride,
                    pixelFormat,
                    (IntPtr)(dibHandle.ToInt64() + Marshal.SizeOf(bitmapInfoHeader))
                );

                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);

                return bitmap;
            }
            finally
            {
                if(dibHandle != IntPtr.Zero)
                {
                    Marshal.FreeCoTaskMem(dibHandle);
                }
            }
        }

        #endregion
        #region 비디오 크기 구하기 - GetVideoSize()

        /// <summary>
        /// 비디오 크기 구하기
        /// </summary>
        /// <returns>비디오 크기</returns>
        public Size GetVideoSize()
        {
            int width;
            int height;

            ThrowExceptionForResult(this.getVideoSizeDelegate(out width, out height), "Failed to get the video size.");

            return new Size(width, height);
        }

        #endregion
        #region 중단하기 - Stop()

        /// <summary>
        /// 중단하기
        /// </summary>
        public void Stop()
        {
            ThrowExceptionForResult(this.stopDelegate(), "Failed to stop a video capture graph.");
        }

        #endregion
        #region 캡처 그래프 제거하기 - DestroyCaptureGraph()

        /// <summary>
        /// 캡처 그래프 제거하기
        /// </summary>
        public void DestroyCaptureGraph()
        {
            this.destroyCaptureGraphDelegate();
        }

        #endregion

        #region 리소스 해제하기 - Dispose()

        /// <summary>
        /// 리소스 해제하기
        /// </summary>
        public void Dispose()
        {
            if(this.moduleHandle != IntPtr.Zero)
            {
                FreeLibrary(this.moduleHandle);

                this.moduleHandle = IntPtr.Zero;
            }

            if(File.Exists(this.moduleFilePath))
            {
                File.Delete(this.moduleFilePath);
            }
        }

        #endregion

        ////////////////////////////////////////////////////////////////////////////////////////// Private

        #region 모듈 로드하기 - LoadModule()

        /// <summary>
        /// 모듈 로드하기
        /// </summary>
        private void LoadModule()
        {
            this.moduleFilePath = Path.GetTempFileName();

            using(FileStream stream = new FileStream(this.moduleFilePath, FileMode.Create, FileAccess.Write))
            {
                using(BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(IsX86Platform ? Resources.DirectShowFacade : Resources.DirectShowFacade64);
                }
            }

            this.moduleHandle = LoadLibrary(this.moduleFilePath);

            if(this.moduleHandle == IntPtr.Zero)
            {
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
        }

        #endregion
        #region 대리자 설정하기 - SetDelegate(modlueHandle)

        /// <summary>
        /// 대리자 설정하기
        /// </summary>
        /// <param name="modlueHandle">모듈 핸들</param>
        private void SetDelegate(IntPtr modlueHandle)
        {
            IntPtr methodHandle = GetProcAddress(modlueHandle, "EnumVideoInputDevices");

            this.enumerateVideoInputDeviceListDelegate = (EnumerateVideoInputDeviceListDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(EnumerateVideoInputDeviceListDelegate));

            methodHandle = GetProcAddress(modlueHandle, "BuildCaptureGraph");

            this.buildCaptureGraphDelegate = (BuildCaptureGraphDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(BuildCaptureGraphDelegate));

            methodHandle = GetProcAddress(modlueHandle, "AddRenderFilter");

            this.addRenderFilterDelegate = (AddRenderFilterDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(AddRenderFilterDelegate));

            methodHandle = GetProcAddress(modlueHandle, "AddCaptureFilter");

            this.addCaptureFilterDelegate = (AddCaptureFilterDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(AddCaptureFilterDelegate));

            methodHandle = GetProcAddress(modlueHandle, "ResetCaptureGraph");

            this.resetCaptureGraphDelegate = (ResetCaptureGraphDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(ResetCaptureGraphDelegate));

            methodHandle = GetProcAddress(modlueHandle, "Start");
            this.startDelegate = (StartDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(StartDelegate));

            methodHandle = GetProcAddress(modlueHandle, "GetCurrentImage");

            this.getCurrentImageDelegate = (GetCurrentImageDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(GetCurrentImageDelegate));

            methodHandle = GetProcAddress(modlueHandle, "GetVideoSize");

            this.getVideoSizeDelegate = (GetVideoSizeDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(GetVideoSizeDelegate));

            methodHandle = GetProcAddress(modlueHandle, "Stop");

            this.stopDelegate = (StopDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(StopDelegate));

            methodHandle = GetProcAddress(modlueHandle, "DestroyCaptureGraph");

            this.destroyCaptureGraphDelegate = (DestroyCaptureGraphDelegate)Marshal.GetDelegateForFunctionPointer(methodHandle, typeof(DestroyCaptureGraphDelegate));            
        }

        #endregion
        #region 결과용 예외 던지기 - ThrowExceptionForResult(resultHandle, message)

        /// <summary>
        /// 결과용 예외 던지기
        /// </summary>
        /// <param name="resultHandle">결과 핸들</param>
        /// <param name="message">메시지</param>
        private static void ThrowExceptionForResult(int resultHandle, string message)
        {
            if(resultHandle < 0)
            {
                throw new DirectShowException(message, resultHandle);
            }
        }

        #endregion
    }
}