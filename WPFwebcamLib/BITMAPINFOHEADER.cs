﻿using System.Runtime.InteropServices;

namespace WPFwebcamLib
{
    /// <summary>
    /// 비트맵 정보 헤더
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BITMAPINFOHEADER
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Field
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region Field

        /// <summary>
        /// 크기
        /// </summary>
        public uint Size;

        /// <summary>
        /// 너비
        /// </summary>
        public int Width;

        /// <summary>
        /// 높이
        /// </summary>
        public int Height;

        /// <summary>
        /// 플레인 수
        /// </summary>
        public ushort PlaneCount;

        /// <summary>
        /// 비트 수
        /// </summary>
        public ushort BitCount;

        /// <summary>
        /// 압축 방법
        /// </summary>
        public uint Compression;

        /// <summary>
        /// 이미지 크기
        /// </summary>
        public uint ImageSize;

        /// <summary>
        /// 미터당 X 픽셀 수
        /// </summary>
        public int XPixelCountPerMeter;

        /// <summary>
        /// 미터당 Y 픽셀 수
        /// </summary>
        public int YPixelCountPerMeter;

        /// <summary>
        /// 사용 색상 인덱스 수
        /// </summary>
        public uint UsedColorCount;

        /// <summary>
        /// 중료 색상 인덱스 수
        /// </summary>
        public uint ImportantColorIndexCount;

        #endregion
    }
}