﻿using System;
using System.Runtime.InteropServices;

namespace WPFwebcamLib
{
    /// <summary>
    /// DirectShow 예외
    /// </summary>
    public sealed class DirectShowException : Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Constructor
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 생성자 - DirectShowException(message, resultHandle)

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="message">메시지</param>
        /// <param name="resultHandle">결과 핸들</param>
        public DirectShowException(string message, int resultHandle) : base(message, Marshal.GetExceptionForHR(resultHandle))
        {
        }

        #endregion
    }
}