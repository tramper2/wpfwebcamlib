﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using Size = System.Drawing.Size;

namespace WPFwebcamLib
{
    /// <summary>
    /// WebCameraControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class WebCameraControl :UserControl
    {
        #region Field

        /// <summary>
        /// 캡처 여부 속성 키
        /// </summary>
        private static readonly DependencyPropertyKey IsCapturingPropertyKey = DependencyProperty.RegisterReadOnly
        (
            "IsCapturing",
            typeof(bool),
            typeof(WebCameraControl),
            new FrameworkPropertyMetadata(false)
        );

        #endregion

        /// <summary>
        /// 캡처 여부 속성
        /// </summary>
        public static readonly DependencyProperty IsCapturingProperty = IsCapturingPropertyKey.DependencyProperty;

        /// <summary>
        /// DirectShow 헬퍼
        /// </summary>
        private DirectShowHelper helper;

        /// <summary>
        /// 비디오 캡처 장치 리스트
        /// </summary>
        private readonly List<VideoCaptureDevice> videoCaptureDeviceList = new List<VideoCaptureDevice>();

        /// <summary>
        /// 캡처 그래프 초기화 여부
        /// </summary>
        private bool captureGraphInitialized;

        /// <summary>
        /// 현재 비디오 캡처 장치
        /// </summary>
        private VideoCaptureDevice currentVideoCaptureDevice;


        #region 캡처 여부 - IsCapturing

        /// <summary>
        /// 캡처 여부
        /// </summary>
        [Browsable(false)]
        public bool IsCapturing
        {
            get
            {
                return (bool)GetValue(IsCapturingProperty);
            }
            private set
            {
                SetValue(IsCapturingPropertyKey, value);
            }
        }

        #endregion

        #region 비디오 크기 - VideoSize

        /// <summary>
        /// 비디오 크기
        /// </summary>
        [Browsable(false)]
        public Size VideoSize
        {
            get
            {
                return IsCapturing ? this.helper.GetVideoSize() : new Size(0, 0);
            }
        }

        #endregion

        #region 생성자 - WebCameraControl()

        /// <summary>
        /// 생성자
        /// </summary>
        public WebCameraControl()
        {
            InitializeComponent();

            this.helper = new DirectShowHelper();

            Loaded += UserControl_Loaded;
        }

        #endregion

        #region 비디오 캡처 장치 리스트 구하기 - GetVideoCaptureDeviceList()

        /// <summary>
        /// 비디오 캡처 장치 리스트 구하기
        /// </summary>
        /// <returns></returns>
        public List<VideoCaptureDevice> GetVideoCaptureDeviceList()
        {
            this.videoCaptureDeviceList.Clear();

            this.helper.EnumerateVideoInputDeviceList(AddVideoCaptureDeviceList);

            return new List<VideoCaptureDevice>(this.videoCaptureDeviceList);
        }

        #endregion
        #region 캡처 시작하기 - StartCapture(videoCaptureDevice)

        /// <summary>
        /// 캡처 시작하기
        /// </summary>
        /// <param name="videoCaptureDevice">비디오 캡처 장치</param>
        public void StartCapture(VideoCaptureDevice videoCaptureDevice)
        {
            if (videoCaptureDevice == null)
            {
                throw new ArgumentNullException();
            }

            if (!this.captureGraphInitialized)
            {
                InitializeCaptureGraph();

                this.captureGraphInitialized = true;
            }

            if (IsCapturing)
            {
                if (this.currentVideoCaptureDevice == videoCaptureDevice)
                {
                    return;
                }

                StopCapture();
            }

            if (this.currentVideoCaptureDevice != null)
            {
                this.helper.ResetCaptureGraph();

                this.currentVideoCaptureDevice = null;
            }

            this.helper.AddCaptureFilter(videoCaptureDevice.DevicePath);

            this.currentVideoCaptureDevice = videoCaptureDevice;

            try
            {
                this.helper.Start();

                IsCapturing = true;
            }
            catch (DirectShowException)
            {
                this.helper.ResetCaptureGraph();

                this.currentVideoCaptureDevice = null;

                throw;
            }
        }

        #endregion
        #region 현재 이미지 구하기 - GetCurrentImage()

        /// <summary>
        /// 현재 이미지 구하기
        /// </summary>
        /// <returns>현재 이미지</returns>
        public Bitmap GetCurrentImage()
        {
            if (!IsCapturing)
            {
                throw new InvalidOperationException();
            }

            return this.helper.GetCurrentImage();
        }

        #endregion
        #region 캡처 중단하기 - StopCapture()

        /// <summary>
        /// 캡처 중단하기
        /// </summary>
        public void StopCapture()
        {
            if (!IsCapturing)
            {
                throw new InvalidOperationException();
            }

            this.helper.Stop();

            IsCapturing = false;

            this.helper.ResetCaptureGraph();

            this.currentVideoCaptureDevice = null;
        }

        #endregion

        #region 컨텐트 변경시 처리하기 - OnContentChanged(oldContent, newContent)

        /// <summary>
        /// 컨텐트 변경시 처리하기
        /// </summary>
        /// <param name="oldContent">이전 컨텐트</param>
        /// <param name="newContent">신규 컨텐트</param>
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            if (oldContent != null)
            {
                throw new InvalidOperationException();
            }
        }

        #endregion

        #region 사용자 컨트롤 로드시 처리하기 - UserControl_Loaded(sender, e)

        /// <summary>
        /// 사용자 컨트롤 로드시 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);

            if (window != null)
            {
                window.Closed += Window_Closed;

                Loaded -= UserControl_Loaded;
            }
        }

        #endregion
        #region 윈도우 닫는 경우 처리하기 - Window_Closed(sender, e)

        /// <summary>
        /// 윈도우 닫는 경우 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            if (this.helper != null)
            {
                if (IsCapturing)
                {
                    StopCapture();
                }

                this.helper.DestroyCaptureGraph();

                this.helper.Dispose();
            }

            this.videoWindow.Dispose();

            Window window = sender as Window;

            if (window != null)
            {
                window.Closed -= Window_Closed;
            }
        }

        #endregion

        #region 비디오 캡처 장치 리스트 추가하기 - AddVideoCaptureDeviceList(info)

        /// <summary>
        /// 비디오 캡처 장치 리스트 추가하기
        /// </summary>
        /// <param name="info">비디오 입력 장치 정보</param>
        private void AddVideoCaptureDeviceList(ref VideoInputDeviceInfo info)
        {
            if (!string.IsNullOrEmpty(info.DevicePath))
            {
                this.videoCaptureDeviceList.Add(new VideoCaptureDevice(info));
            }
        }

        #endregion
        #region 캡처 그래프 초기화하기 - InitializeCaptureGraph()

        /// <summary>
        /// 캡처 그래프 초기화하기
        /// </summary>
        private void InitializeCaptureGraph()
        {
            this.helper.BuildCaptureGraph();

            this.helper.AddRenderFilter(this.videoWindow.Handle);
        }

        #endregion
    }
}
