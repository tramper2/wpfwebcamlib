﻿using System.Runtime.InteropServices;

namespace WPFwebcamLib
{
    /// <summary>
    /// 사각형
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Field
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region Field

        /// <summary>
        /// 왼쪽
        /// </summary>
        public int Left;
        
        /// <summary>
        /// 위쪽
        /// </summary>
        public int Top;
        
        /// <summary>
        /// 오른쪽
        /// </summary>
        public int Right;
        
        /// <summary>
        /// 아래쪽
        /// </summary>
        public int Bottom;

        #endregion
    }
}