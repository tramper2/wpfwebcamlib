﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace WPFwebcamLib
{
    /// <summary>
    /// 비디오 윈도우
    /// </summary>
    public class VideoWindow : HwndHost
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////// Import
        ////////////////////////////////////////////////////////////////////////////////////////// Static
        //////////////////////////////////////////////////////////////////////////////// Private

        #region 윈도우 생성하기 - CreateWindowEx(style, className, windowName, windowStyles, x, y, width, height, parentWindowHandle, menuHandle, instanceHandle, parameter)

        /// <summary>
        /// 윈도우 생성하기
        /// </summary>
        /// <param name="style">스타일</param>
        /// <param name="className">클래스명</param>
        /// <param name="windowName">윈도우명</param>
        /// <param name="windowStyles">윈도우 스타일</param>
        /// <param name="x">X 좌표</param>
        /// <param name="y">Y 좌표</param>
        /// <param name="width">너비</param>
        /// <param name="height">높이</param>
        /// <param name="parentWindowHandle">부모 윈도우 핸들</param>
        /// <param name="menuHandle">메뉴 핸들</param>
        /// <param name="instanceHandle">인스턴스 핸들</param>
        /// <param name="parameter">매개 변수</param>
        /// <returns>처리 결과</returns>
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr CreateWindowEx(uint style, string className, string windowName, WindowStyles windowStyles, int x, int y, int width, int height, IntPtr parentWindowHandle, IntPtr menuHandle, IntPtr instanceHandle, IntPtr parameter);

        #endregion
        #region 클라이언트 사각형 구하기 - GetClientRect(windowHandle, rectangle)

        /// <summary>
        /// 클라이언트 사각형 구하기
        /// </summary>
        /// <param name="windowHandle">윈도우 핸들</param>
        /// <param name="rectangle">사각형</param>
        /// <returns>처리 결과</returns>
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern Boolean GetClientRect(IntPtr windowHandle, out RECT rectangle);

        #endregion
        #region 윈도우 제거하기 - DestroyWindow(windowHandle)

        /// <summary>
        /// 윈도우 제거하기
        /// </summary>
        /// <param name="windowHandle">윈도우 핸들</param>
        /// <returns>처리 결과</returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern Boolean DestroyWindow(IntPtr windowHandle);

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Property
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region Field

        /// <summary>
        /// 윈도우 핸들
        /// </summary>
        private IntPtr windowHandle = IntPtr.Zero;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Property
        ////////////////////////////////////////////////////////////////////////////////////////// Public

        #region 핸들 - Handle

        /// <summary>
        /// 핸들
        /// </summary>
        public new IntPtr Handle
        {
            get
            {
                return this.windowHandle;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////// Method
        ////////////////////////////////////////////////////////////////////////////////////////// Protected

        #region 윈도우 구축하기 (코어) - BuildWindowCore(parentWindowHandle)

        /// <summary>
        /// 윈도우 구축하기 (코어)
        /// </summary>
        /// <param name="parentWindowHandle">부오 윈도우 핸들</param>
        /// <returns>윈도우 핸들 참조</returns>
        protected override HandleRef BuildWindowCore(HandleRef parentWindowHandle)
        {
            RECT clientArea;

            GetClientRect(parentWindowHandle.Handle, out clientArea);

            this.windowHandle = CreateWindowEx
            (
                0,
                "Static",
                "",
                WindowStyles.WS_CHILD | WindowStyles.WS_VISIBLE,
                0,
                0,
                clientArea.Right - clientArea.Left,
                clientArea.Bottom - clientArea.Top,
                parentWindowHandle.Handle,
                IntPtr.Zero,
                IntPtr.Zero,
                IntPtr.Zero
            );

            return new HandleRef(this, this.windowHandle);
        }

        #endregion
        #region 윈도우 제거하기 (코어) - DestroyWindowCore(windowHandle)

        /// <summary>
        /// 윈도우 제거하기 (코어)
        /// </summary>
        /// <param name="windowHandleRef">윈도우 핸들 참조</param>
        protected override void DestroyWindowCore(HandleRef windowHandleRef)
        {
            DestroyWindow(windowHandleRef.Handle);
        }

        #endregion
    }
}