﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using WPFwebcamLib;

namespace WPFWebcamTest
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.videoCaptureDeviceComboBox.ItemsSource = this.webCameraControl.GetVideoCaptureDeviceList();

            if (this.videoCaptureDeviceComboBox.Items.Count > 0)
            {
                this.videoCaptureDeviceComboBox.SelectedItem = this.videoCaptureDeviceComboBox.Items[0];
            }

            this.videoCaptureDeviceComboBox.SelectionChanged += videoCaptureDeviceComboBox_SelectionChanged;
            this.startButton.Click += startButton_Click;
            this.stopButton.Click += stopButton_Click;
            this.imageButton.Click += imageButton_Click;
        }

        #region 비디오 캡처 장치 콤보 박스 선택 변경시 처리하기 - videoCaptureDeviceComboBox_SelectionChanged(sender, e)

        /// <summary>
        /// 비디오 캡처 장치 콤보 박스 선택 변경시 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void videoCaptureDeviceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.startButton.IsEnabled = e.AddedItems.Count > 0;
        }

        #endregion
        #region 시작하기 버튼 클릭시 처리하기 - startButton_Click(sender, e)

        /// <summary>
        /// 시작하기 버튼 클릭시 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            VideoCaptureDevice videoCaptureDevice = (VideoCaptureDevice)this.videoCaptureDeviceComboBox.SelectedItem;

            this.webCameraControl.StartCapture(videoCaptureDevice);

            this.startButton.IsEnabled = false;
        }

        #endregion
        #region 중단하기 버튼 클릭시 처리하기 - startButton_Click(sender, e)

        /// <summary>
        /// 중단하기 버튼 클릭시 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            this.webCameraControl.StopCapture();

            this.startButton.IsEnabled = true;
        }

        #endregion
        #region 이미지 버튼 클릭시 처리하기 - startButton_Click(sender, e)

        /// <summary>
        /// 이미지 버튼 클릭시 처리하기
        /// </summary>
        /// <param name="sender">이벤트 발생자</param>
        /// <param name="e">이벤트 인자</param>
        private void imageButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog { Filter = "비트맵 이미지|*.bmp" };

            if (saveFileDialog.ShowDialog() == true)
            {
                this.webCameraControl.GetCurrentImage().Save(saveFileDialog.FileName);
            }
        }

        #endregion
    }
}
